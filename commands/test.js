const CommandBase = require("../src/CommandBase");

class TestCommand extends CommandBase {
    constructor() {
      super("test", ["t"], "A test command");
    }
  
    run(msg) {
        msg.reply("Test command");
    }
}

module.exports = TestCommand;