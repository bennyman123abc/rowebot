/*   Notes for us
*
*   Class Names:  Uppercase:Uppercase (CommandParser)
*   Variables:    lowercase:Uppercase (someVariable)
*
*/

// All the requirements
const Discord = require('discord.js')
const Client = new Discord.Client()
const fs = require('fs')
const ConfigHandler = require('./src/util/ConfigHandler')
const Logger = require('./src/util/Logger')
const CommandLoader = require('./src/util/CommandLoader')
const SQLDriver = require('./src/util/SQLDriver')

// lol xd?

// variables
var config = process.env // Will be changed to a file handled by the config handler
var commandLoader = new CommandLoader('./commands')
var dataDB = new SQLDriver('./data/data.sqlite')

// just in case we need it
var commands = commandLoader.commands

// Event Handlers
Client.on('ready', () => {
  console.log(`Logged in as ${Client.user.tag}!`)
})

Client.on('guildCreate', function (guild) {
  dataDB.schemas.guild.Create({
    guildId: guild.id,
    guildName: guild.name,
    prefix: '!'
  })
})

Client.on('message', function (msg) {
  let commandInvoker = '!' // tmep
  let cmd = msg.content.split(' ').slice(0, 1).join('').replace(commandInvoker, '')
  let args = msg.content.split(' ').slice(1)

  if (cmd == 'ping') {
    if (args[0] == 'error') {
      Logger.error('Oops, You fucked up')
    }
    if (args[0] == 'warn') {
      Logger.warn('Just a friendly reminder, The engine is on fire')
    }
    if (args[0] == 'fatal') {
      Logger.fatal('OH NOZIES')
    }
    if (args[0] == 'info') {
      Logger.info('All is good, in the neighborhood')
    }
    msg.reply('Pong!')
  }
})

Client.login(config.token)
