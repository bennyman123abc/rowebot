const Chalk = require('chalk');

module.exports.info = function(msg) {
    console.log("[INFO] " + msg);
}

// Orange/Yellow
module.exports.warn = function(msg) {
    console.log(Chalk.hex('#F5B041')("[WARN] " + msg));
}

// Light red
module.exports.error = function(msg) {
    console.log(Chalk.hex('#CD6155')("[ERROR] " + msg));
}

// ur pc diez and i mean it
module.exports.fatal = function(msg) {
    console.log(Chalk.hex('#922B21')("[YOU FUCKED UP] " + msg));

}