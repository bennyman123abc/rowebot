function isDirSync(path) {
    try {
        return fs.lstatSync(path).isDirectory();
    }
    catch(e) {
        return false;
    }
}

module.exports = {
    isDirSync
};