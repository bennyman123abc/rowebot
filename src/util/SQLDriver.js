const Sequelize = require("sequelize");

class SQLDriver {
    constructor(filename) {
        this.schemas = {};
        this.db = new Sequelize('database', 'username', 'password', {
            host: 'localhost',
            dialect: 'sqlite',
          
            pool: {
              max: 5,
              min: 0,
              acquire: 30000,
              idle: 10000
            },
          
            // SQLite only
            storage: filename,
          
            // http://docs.sequelizejs.com/manual/tutorial/querying.html#operators
            operatorsAliases: false
          });

        this.defineSchema('guild', {
            guildName: Sequelize.STRING,
            guildId: Sequelize.STRING,
            prefix: Sequelize.STRING
        });

        this.defineSchema('user', {
            username: Sequelize.STRING,
            userId: Sequelize.STRING,
            coins: Sequelize.INTEGER,
            xp: Sequelize.INTEGER
        });
    }

    defineSchema(name, data) {
        this.schemas[name] = (this.db.define(name, data));
    }
}

module.exports = SQLDriver;