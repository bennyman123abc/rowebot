const fs = require("fs");
const createStatus = require("./Status").createStatus;
const Logger = require("./Logger");
const utils = require("./Utils")
const path = require("path");

class CommandLoader {
    constructor(dir) {
        this.dir = dir;
        this.commands = [];

        this.load();
    }

    load() {

        var dir = this.dir;
        // if (!utils.isDirSync(this._dir)) {
        //     Logger.error("Provided directory does not exist.")
        //     Logger.info(this._dir);
        //     return createStatus(-1, "Provided directory does not exist.");
        // }

        fs.readdirSync(this.dir).forEach(function(command){
            try {
                var commandName = command;
                var command = require(path.join(__dirname, "commands", command));
            }
            catch(e) {
                Logger.error(`Error loading command, '${command}'.`);
                Logger.error(e);
                return createStatus(-1, `Error loading command, '${command}'.`);
            }

            try {
                command = new command();
            }
            catch(e) {
                Logger.error(`Error initializing command, '${commandName}'.`)
                return createStatus(-1, `Error initializing command, '${commandName}'.`);
            }

            if (!command) return;

            this.commands.set(command.data.name, command);
        });
    }
}

module.exports = CommandLoader;
