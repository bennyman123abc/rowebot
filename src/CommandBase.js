const Util = require('./util/Util');

class CommandBase {
  constructor(_n, _ali, _h){
    this.name = _n
    this.aliases = _ali
    this.help = _h
  }

  run(msg, args){
     Util.Log(2, "Command " + this.name + " didn't have a Command set!");
  }
}

module.exports = CommandBase;